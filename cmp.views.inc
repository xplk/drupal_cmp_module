<?php

/**
 * @file
 * GMap Views support.
 */

/**
 * Preprocess function for
 */
function template_preprocess_cmp_view_style(&$vars) {
   initThemeClasses($vars);
}

function initThemeClasses(&$vars){
    //set theming variables and classes!!!!!!! TODO: when: functional implemented
    $view     = $vars['view'];
    $result   = $view->result;
    $options  = $view->style_plugin->options;
    $handler  = $view->style_plugin;
    $default_row_class = isset($options['default_row_class']) ? $options['default_row_class'] : TRUE;
    $row_class_special = isset($options['row_class_special']) ? $options['row_class_special'] : TRUE;


    $columns  = 4;

    $rows = array();
    $row_indexes = array();

    $row = array();
    $col_count = 0;
    $row_count = 0;
    $count = 0;
    foreach ($vars['rows'] as $row_index => $item) {
        $count++;
        $row[] = $item;
        $row_indexes[$row_count][$col_count] = $row_index;
        $col_count++;
        if ($count % $columns == 0) {
            $rows[] = $row;
            $row = array();
            $col_count = 0;
            $row_count++;
        }
    }
    if ($row) {
        $rows[] = $row;
    }


    // Apply the row classes
    foreach ($rows as $row_number => $row) {
        $row_classes = array();
        if ($default_row_class) {
            $row_classes[] =  'row-' . ($row_number + 1);
        }
        if ($row_class_special) {
            if ($row_number == 0) {
                $row_classes[] =  'row-first';
            }
            if (count($rows) == ($row_number + 1)) {
                $row_classes[] =  'row-last';
            }
        }
        $vars['row_classes'][$row_number] = implode(' ', $row_classes);
        foreach ($rows[$row_number] as $column_number => $item) {
            $column_classes = array();
            if ($default_row_class) {
                $column_classes[] = 'col-'. ($column_number + 1);
            }
            if ($row_class_special) {
                if ($column_number == 0) {
                    $column_classes[] = 'col-first';
                }
                elseif (count($rows[$row_number]) == ($column_number + 1)) {
                    $column_classes[] = 'col-last';
                }
            }
            if (isset($row_indexes[$row_number][$column_number]) && $column_class = $view->style_plugin->get_row_class($row_indexes[$row_number][$column_number])) {
                $column_classes[] = $column_class;
            }
            $vars['column_classes'][$row_number][$column_number] = implode(' ', $column_classes);
        }
    }
    $vars['rows'] = $rows;
    $vars['class'] = 'views-view-grid cols-' . $columns;

    // Add the summary to the list if set.
    if (!empty($handler->options['summary'])) {
        $vars['attributes_array'] = array('summary' => filter_xss_admin($handler->options['summary']));
    }

    // Add the caption to the list if set.
    if (!empty($handler->options['caption'])) {
        $vars['caption'] = filter_xss_admin($handler->options['caption']);
    }
    else {
        $vars['caption'] = '';
    }
}