<style type="text/css">
    td{
        text-align: center !important;
        width: 200px !important;
        vertical-align: top !important;
    }
    .wide{
        width: 100px;
    }
    select{
        width: 100% !important;
    }
    .control-label{
        display: inline;
        float: left;
    }
    .controls{
        float: inherit;
        text-align: right;
    }
    legend{
        border-bottom: 0px;
        margin: 0px;
    }
</style>
<table class="views-table sticky-enabled table sticky-table" border="1px">
    <tr>
        <td class="views-field views-align-center ">
                <?php print t('Weight'); ?>
        </td>
        <td class="views-field views-align-center">
            <?php print t('Parameter'); ?>
        </td>
        <td class="views-field views-align-center">
            <div class='wide'>
            <?php print t('Rule to compare'); ?>
            </div>
        </td>
        <td class="views-field views-align-center">
            <?php print t('Parameter best value'); ?>
        </td>

    </tr>
    <?php foreach($form['#fields'] as $field_name): ?>
        <tr>
            <td class="views-field views-align-center">
                <?php print drupal_render($form[$field_name.'_pond']);?>
            </td>
            <td class="views-field views-align-center">
                <?php print $form['#fields_human'][$field_name]; ?>
                <h6>(more common value: <?php print $form['#fields_avg'][$field_name]; ?> )</h6>
            </td>
            <td class="views-field views-align-center">
                <?php print drupal_render($form[$field_name.'_rule_compare']); ?>
            </td>
            <td class="views-field views-align-center">
                <?php print drupal_render($form[$field_name.'_rule_best_value']); ?>
            </td>

        </tr>
    <?php endforeach; ?>
</table>

<?php print drupal_render($form['form_build_id']); ?>
<?php print drupal_render($form['form_id']); ?>
<?php print drupal_render($form['form_token']); ?>
<?php print drupal_render($form['submit']); ?>
<?php print drupal_render($form['results']); ?>