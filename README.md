It's a module for drupal for compare custom products (tt.bench.project)
========================================

1) Installation
---------------

### a) Check your System Configuration

Before you begin, make sure that your local system is properly configured for Drupal. 

### b) Create a module folder

	$ create a 'cmp' folder in  ./sites/all/modules 

### c) Clone the git Repository from the main repository or fork it to your github account:

	$ cd path/to/drupal/sites/all/modules/cmp
	$ git clone https://xplk@bitbucket.org/xplk/drupal_cmp_module.git

### d) Use view format 'Comparator' to get module functionality
### e) alter needed preprocesses to proper theming

