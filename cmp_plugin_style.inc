<?php
/**
 * Style plugin to render a compare.
 *
 * @ingroup views_style_plugins
 */
class cmp_plugin_style extends views_plugin_style{
    /**
     * Set default options
     */
    function option_definition() {
        $options = parent::option_definition();
        return $options ;
    }

    /**
     * Render the given style.
     */
    function options_form(&$form, &$form_state) {
       parent::options_form($form, $form_state);
    }
}