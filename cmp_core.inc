<?php

function cmp_form_calc_result(&$form,&$form_state){
    $compare_object = $_SESSION['cmp_data_object'];
    $results = array();
    foreach($compare_object['nodes'] as $node){
        $node_title = $node['node']->title;

        $results[$node_title] = array();

        foreach($node['fields'] as $field_name => $field){
            if(!isset($field['value'])){
                break;
            }
            $multiply = $form_state['values'][$field_name.'_pond'];
            switch($field['type']){
                case 'number':
                    $best_value = $form_state['values'][$field_name.'_rule_best_value'];
                    $value = $field['value'];

                    $rule = $form_state['values'][$field_name.'_rule_compare'];
                    switch($rule){
                        case 'more':
                            if($value >= $best_value){
                                $rate = ($value/$best_value*100)*$multiply;
                            }else{
                                $rate = 0;
                            }
                            break;
                        case 'less':
                            if($value <= $best_value){
                                $rate = ($best_value/$value*100)*$multiply;
                            }else{
                                $rate = 0;
                            }
                            break;
                        case 'exactly':
                            if($value == $best_value){
                                $rate = 100*$multiply;
                            }else{
                                $rate = 0;
                            }
                            break;
                        case 'near':
                            $near_value = $form_state['values'][$field_name.'_near_value']; //validate to positive
                            if(abs($value - $best_value)/$near_value*100 <= 100 ){
                                $rate = (100- (abs($value - $best_value)/$near_value)*100)*$multiply;
                            }else{
                                $rate = 0;
                            }
                            break;
                        default:
                            $rate = 0;
                    }
                    break;
                case 'list':

                    $value = $field['value'];
                    $rule = $form_state['values'][$field_name.'_rule_compare'];
                    switch($rule){
                        case 'exists':
                            if($value > 0){
                                $rate = 100*$multiply;
                            }else{
                                $rate = 0;
                            }
                            break;
                        case 'not_exists':
                            if($value == 0){
                                $rate = 100*$multiply;
                            }else{
                                $rate = 0;
                            }
                            break;
                        case 'exactly':
                            $best_value = $form_state['values'][$field_name.'_rule_best_value'];
                            if($value == $best_value){
                                $rate = 100*$multiply;
                            }else{
                                $rate = 0;
                            }
                            break;
                        default:
                            $rate = 0;
                    }
                    break;
                case 'taxonomy':
                    $value = $field['value'];
                    $rule = $form_state['values'][$field_name.'_rule_compare'];
                    switch($rule){
                        case 'by_rate':
                            $best_values = $form_state['values'][$field_name.'_rule_rate'];
                            if(array_key_exists($value,$best_values)){
                                $rate=$best_values[$value]*$multiply;
                            }else{
                                $rate=0;
                            }
                            break;
                        case 'exactly':
                        $best_values = $form_state['values'][$field_name.'_rule_best_value'];
                            if(array_key_exists($value,$best_values)){
                                $rate=100*$multiply;
                            }else{
                                $rate=0;
                            }
                            break;
                        default:
                            $rate = 0;
                    }
                    break;
                default:
                    $rate = 0;
            }
            $results[$node_title]['rate']+=$rate;
            $results[$node_title]['nid']=$node['node']->nid;
        }

    }
    $best['best_value'] = 0;
    $best['avg'] = 0;
    $best['worse_value'] = PHP_INT_MAX;
    foreach($results as $result_title => $result){
        $best['avg'] += $result['rate']/count($results);
        if($result['rate'] > $best['best_value']){
            $best['best_title'] = $result_title;
            $best['best_value'] = $result['rate'];
            $best['best_nid'] = $result['nid'];
        }
        if($result['rate'] < $best['worse_value']){
            $best['worse_title'] = $result_title;
            $best['worse_value'] = $result['rate'];
            $best['worse_nid'] = $result['nid'];
        }
    }


    $summary = null;
    if(isset($best['best_nid'])){
        $best_link = '<a href="'.url('node/' . $best['best_nid'], array('absolute'=>TRUE)).'">' .$best['best_title'].'</a>';
        $worse_link = '<a href="'.url('node/' . $best['worse_nid'], array('absolute'=>TRUE)).'">' .$best['worse_title'].'</a>';
        $summary = '<p>Best is: '. $best_link .' with result: '.$best['best_value'].'</p>'.
               '<p>Worse is: '. $worse_link .' with result: '.$best['worse_value'].'</p>'.
               '<p>Average rating: '.$best['avg'].'</p>';
    }else{
        $summary = 'No results...';
    }

    $form['results'] = array(
        '#type' => 'fieldset',
        '#title' => 'Results',
        '#collapsible' => FALSE,
        '#collapsed' => FALSE,
        '#tree' => TRUE,
        '#prefix' => '<h3 id="results">',
        '#suffix' => '</h3>',
    );
    $form['results']['summary'] = array(
        '#markup' =>$summary,
    );
    $form['results']['all'] = array(
        '#type' => 'fieldset',
        '#title' => 'More detail',
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
        '#tree' => TRUE,
    );
    foreach($results as $result_title => $result){
        $link = '<a href="'.url('node/' . $result['nid'], array('absolute'=>TRUE)).'">' .$result_title.'</a>';
        $form['results']['all'][$result_title] = array(
            '#markup' =>'<p>'.$link.' rate: '. $result['rate'].'</p>',
        );
    }
    return $form;
 }

function initCompareVariables(&$vars){
    $type = node_load($vars['view']->result[0]->nid)->type;
    $compare_fields = field_info_instances("node", $type);
    $bundle = null;
    $nodes = array();

    foreach($vars['view']->result as $result){
        $nodes[] = node_load($result->nid);
    }
    $compare_objects = array();
    foreach($nodes as $node){
        $object = array();
        foreach($compare_fields as $field){
            if(!isset($bundle)){
                $bundle = $field['bundle'];
            }

            $items = (array)field_get_items('node',$node,$field['field_name']); //WARNING BAGOFIX
            foreach($items as $item){
                if(isset($item['value'])){
                    $object['fields'][$field['field_name']]['value']=$item['value'];
                }elseif(isset($item['tid'])){
                    $object['fields'][$field['field_name']]['value']=$item['tid'];
                    $tree = taxonomy_get_tree(taxonomy_term_load($item['tid'])->vid);
                    foreach($tree as $term){
                        $object['fields'][$field['field_name']]['list'][$term->name]['vid']=$term->vid;
                        $object['fields'][$field['field_name']]['list'][$term->name]['tid']=$term->tid;
                    }
                }
            }
            $object['fields'][$field['field_name']]['type']=$compare_fields[$field['field_name']]['display']['default']['module'];
            $object['fields'][$field['field_name']]['label'] = $field['label'];

            $object['node'] = $node;
        }
        $compare_objects['nodes'][] = $object;
    }
    foreach($compare_objects['nodes'][0]['fields'] as $field_name => $field){
        $avg = null;
        $values = array();
        foreach($compare_objects['nodes'] as $node){
            switch($node['fields'][$field_name]['type']){
               case 'number':
                   $avg += intval($node['fields'][$field_name]['value']/count($compare_objects['nodes']));
                   break;
               case 'list':

                   if(isset($values[$node['fields'][$field_name]['value']])){
                      $values[$node['fields'][$field_name]['value']] +=1;
                        if($avg <= $values[$node['fields'][$field_name]['value']]){
                            $avg = $node['fields'][$field_name]['value'];
                        }
                   }else{
                       $values[$node['fields'][$field_name]['value']] =1;
                   }
                   break;
                case 'taxonomy':

                    if(isset($values[$node['fields'][$field_name]['value']])){
                        $values[$node['fields'][$field_name]['value']] +=1;
                        if($avg <= $values[$node['fields'][$field_name]['value']]){
                            $avg = taxonomy_term_load($node['fields'][$field_name]['value'])->name;
                        }
                    }else{
                        $values[$node['fields'][$field_name]['value']] =1;
                    }
                    break;
                default:
               $avg = 'unreachable';
            }
        }

        $compare_objects['fields_avg'][$field_name] = $avg;
    }

    $compare_objects['bundle'] = $bundle;
    if(!isset($_SESSION['cmp_data_result'])){
        $compare_objects['result'] = t('Submit your criteria for results');
    }else{
        $compare_objects['result'] = $_SESSION['cmp_data_result'];
    }
    $compare_objects['callback_url'] = 'secret/cmp';
    $vars['compare_objects'] = $compare_objects;
    $_SESSION['cmp_data_object'] = $compare_objects;
}